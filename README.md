# CRUD Base Class and Controller #

This package contains abstract CRUD updater class and controller for use with Laravel
resource routes.

### Requirements ###

* PHP >=7.2
* Laravel >= 5.8

### Setup ###

* Include in project **composer.json**


```
#!javascript

    {
        "require": {
            "micron/laravel-crud": "dev-master"
        },
        "repositories": [
            {
                "type": "vcs",
                "url":  "git@bitbucket.org:micronresearch/laravel-crud.git"
            }
        ]
    }
```

### Older Versions: Use 1.* ###

* PHP >=5.5.9
* Laravel >= 5.1

```
#!javascript

    {
        "require": {
            "micron/laravel-crud": "1.*"
        },
    }
```

_**NOTE:** ATLAS 1.03.000 matches tag v1.00.001_