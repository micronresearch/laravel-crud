<?php
namespace Micron\Controllers\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Micron\Repositories\Interfaces\CrudRepository;
use InvalidArgumentException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Illuminate\Http\Response;

/**
 * Trait CrudAPI
 *
 * @package Micron\Controllers\Traits
 *
 * Requirements of use:
 *      $this->repository MUST be set and be an instance of Micron\Repositories\Interfaces\CrudRepository
 *      $this->allow MUST be set to explicitly allow methods within the controller
 *
 * @property array $allow ['index', 'store', 'show', 'update', 'destroy', 'count']
 * @property CrudRepository $repository
 */
trait CrudAPI
{
    /* Class Validation Methods ------------------------------------------------------------------------------------- */

    /**
     * Crud API Trait Allow
     *
     * This is used throughout the trait, ensures that the model property has been correctly set.
     *
     * @param string $method
     * @return bool
     * @throws InvalidArgumentException
     * @throws MethodNotAllowedException
     */
    protected function crudAPITraitAllowed($method)
    {
        if (!isset($this->allow) || !is_array($this->allow)) {
            throw new InvalidArgumentException('Allow property not set for '.self::class);
        } elseif (!in_array($method, $this->allow)) {
            throw new MethodNotAllowedException($this->allow);
        }
        return true;
    }

    /**
     * Crud Repository Trait Model
     *
     * This is used throughout the trait, ensures that the repository property has been correctly set.
     *
     * @return CrudRepository
     * @throws InvalidArgumentException
     */
    protected function crudAPITraitRepository()
    {
        if (!isset($this->repository) || empty($this->repository)) {
            throw new InvalidArgumentException('Repository not set for '.self::class);
        } elseif (!is_object($this->repository)
            || !in_array(CrudRepository::class, class_implements($this->repository))
        ) {
            throw new InvalidArgumentException('Repository invalid for '.self::class);
        }
        return $this->repository;
    }

    /* Public Methods ----------------------------------------------------------------------------------------------- */

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response|Collection
     */
    public function index(Request $request)
    {
        $this->crudAPITraitAllowed('index');
        $params = $request->all();
        return $this->repository->loadCollection($params);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Request  $request
     * @return Response|Collection
     */
    public function count(Request $request)
    {
        $this->crudAPITraitAllowed('count');
        $params = $request->all();
        return $this->repository->countCollection($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response|Model
     */
    public function store(Request $request)
    {
        $this->crudAPITraitAllowed('store');
        return $this->repository->addEntry($this->restrictRequestDataBeforeSave($request));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response|Model
     */
    public function show($id, Request $request)
    {
        $this->crudAPITraitAllowed('show');
        return $this->repository->getEntry($id, $request->all());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param  Request  $request
     * @return Response|Model
     */
    public function update($id, Request $request)
    {
        $this->crudAPITraitAllowed('update');
        return $this->repository->updateEntry($id, $this->restrictRequestDataBeforeSave($request));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response|Model
     */
    public function destroy($id)
    {
        $this->crudAPITraitAllowed('destroy');
        return $this->repository->deleteEntry($id);
    }

    /* Protected Methods -------------------------------------------------------------------------------------------- */

    /**
     * Restrict Request Data Before Save
     *
     * This is based on the Request rules (if applicable) but this method can be overridden if required.
     * Note: If a parameter is passed but does not require validation, it still needs to be included in the
     *       rules array (just leave empty) for it to be passed to the repository to be saved.
     *
     * @param Request $request
     * @param null|array $only
     *
     * @return array
     */
    protected function restrictRequestDataBeforeSave(Request $request, $only = null)
    {
        if (is_array($only)) {
            return $request->only($only);
        }
        // Else
        if (method_exists($request, 'rules') && !empty($request->rules())) {
            return $request->only(array_keys($request->rules()));
        }
        // Else
        return $request->all();
    }
}
