<?php
namespace Micron\Events\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface CrudChangeEvent
{
    /**
     * CrudEvent constructor.
     *
     * @param Model $entry
     * @param Model $user
     * @param string $notes
     */
    public function __construct(Model $entry, Model $user, $notes = null);
}
