<?php

namespace Micron\Events\Traits;

use Illuminate\Broadcasting\Channel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Auth\Authenticatable;

trait CrudChangeEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Model
     */
    public $entry;

    /**
     * @var Model
     */
    public $user;

    /**
     * @var string|null
     */
    public $notes;

    /**
     * Create a new event instance.
     *
     * @param Model $entry
     * @param Model|Authenticatable $user
     * @param string|null $notes
     */
    public function __construct(Model $entry, Model $user, $notes = null)
    {
        $this->entry = $entry;
        $this->user = $user;
        $this->notes = $notes;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
