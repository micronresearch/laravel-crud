<?php
namespace Micron\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

interface CrudRepository
{
    /**
     * Load Collection
     *
     * @param array $params
     * @param null|string $orderBy
     * @param null|Model|Builder $query
     *
     * @return Collection
     */
    public function loadCollection($params, $orderBy = null, $query = null);

    /**
     * Count Collection
     *
     * @param $params
     * @param null|Model|Builder $query
     *
     * @return mixed
     */
    public function countCollection($params, $query = null);

    /**
     * Get Entry
     *
     * @param int $id
     * @param array|null $params
     *
     * @return Model
     */
    public function getEntry($id, $params = null);

    /**
     * Add Entry
     *
     * @param array $params
     *
     * @return Model
     */
    public function addEntry($params);

    /**
     * Update Entry
     *
     * @param int $id
     * @param array $params
     *
     * @return Model
     */
    public function updateEntry($id, $params);

    /**
     * Delete Entry
     *
     * @param int $id
     *
     * @return Model
     */
    public function deleteEntry($id);

    /**
     * New Model
     *
     * @return Model
     */
    public function newModel();

    /**
     * New Query
     *
     * @return Model|Builder
     */
    public function newQuery();
}
