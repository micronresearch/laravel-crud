<?php
namespace Micron\Repositories\Traits;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use InvalidArgumentException;
use Illuminate\Support\Collection;
use Micron\Events\Interfaces\CrudChangeEvent;
use Illuminate\Support\Str;
use \BadMethodCallException;
use \Exception;

/**
 * Trait CrudRepository
 *
 * @package Micron\Repositories\Traits
 *
 * Requirements of use:
 *      $this->model MUST be set and be an instance of Illuminate\Database\Eloquent\Model
 *      $this->searchKeys SHOULD be set to array of fields that search will be matched to
 *      $this->defaultOrderBy CAN be set to Query builder order by default value
 *      $this->changeEvents CAN be set to array of ['methodName' => 'Event::class', ...]
 *
 * Note: This class currently only fires events for changes (update, add, delete)
 * If more events are needed they should be included as a separate type of event.
 *
 * @property Model $model
 * @property array $searchKeys
 * @property array|string $defaultOrderBy
 * @property array $changeEvents
 */
trait CrudRepository
{
    protected $muteNextChangeEvent = false;
    protected $allowedWith = null;

    /* Class Validation Methods ------------------------------------------------------------------------------------- */

    /**
     * Crud Repository Trait Model
     *
     * This is used throughout the trait, ensures that the model property has been correctly set.
     *
     * @return Model|Builder
     * @throws InvalidArgumentException
     */
    protected function crudRepositoryTraitModel()
    {
        if (!isset($this->model)
            || empty($this->model)
            || !is_object($this->model)
            || !is_subclass_of($this->model, Model::class)
        ) {
            throw new InvalidArgumentException();
        }
        return $this->model->newInstance();
    }

    /* Public Methods ----------------------------------------------------------------------------------------------- */

    /**
     * Load Collection
     *
     * @param array $params
     * @param string $orderBy
     * @param Model|Builder $query - Added as a parameter so this method can be extended
     *
     * @return Collection
     */
    public function loadCollection($params, $orderBy = null, $query = null)
    {
        $params = $this->transformParameterCase($params);
        $query = $this->loadCollectionQuery($params, $orderBy, $query);

        $fields = (array_key_exists('fields', $params)) ?
            (is_array($params['fields'])? $params['fields']: explode(',', $params['fields']))
            : ['*'];

        /** @var Paginator|\Illuminate\Support\Collection $return */
        if (array_key_exists('per_page', $params)) {
            $return = $query->paginate($params['per_page']);
        } else {
            $return = $query->get();
        }

        if ($fields) {
            $return->each(function (Model $item, $key) use ($return, $fields) {
                if ($fields[0] === '*') {
                    $return->put($key, $item);
                } else {
                    $return->put($key, $item->only($fields));
                }
            });
        }

        return $return;
    }

    /**
     * Count Collection
     *
     * @param $params
     * @param null|Model|Builder $query
     *
     * @return mixed
     */
    public function countCollection($params, $query = null)
    {
        unset($params['limit']); // Remove limit from query if it exists
        $params = $this->transformParameterCase($params);
        $query = $this->loadCollectionQuery($params, null, $query);
        return $query->count();
    }

    /**
     * Get Entry
     *
     * @param int $id
     * @param array|null $params
     *
     * @return Model
     */
    public function getEntry($id, $params = null)
    {
        $data = $this->newQuery();

        if ($params) {
            if (array_key_exists('with', $params)) {
                $data = $this->queryWith($data, $params['with']);
            }
        }
        return $data->findOrFail($id);
    }

    /**
     * Add Entry
     *
     * @param array $params
     *
     * @return Model
     */
    public function addEntry($params)
    {
        $newEntry = $this->crudRepositoryTraitModel()->newInstance($params); /** @var Model $newEntry */
        $this->setNow('created_at', $newEntry, $params);
        $this->setNow('updated_at', $newEntry, $params);
        $newEntry->save();
        $entry = $this->getEntry($newEntry->id);
        $this->triggerChangeEvent('addEntry', $entry);
        return $entry;
    }

    /**
     * Update Entry
     *
     * @param int $id
     * @params array $params
     *
     * @return Model
     */
    public function updateEntry($id, $params)
    {
        $entry = $this->crudRepositoryTraitModel()->findOrFail($id); /** @var Model $entry */
        $notes = $this->hasEventTrigger('updateEntry')?$this->changes($entry, $params):null;
        $params = $this->setNow('updated_at', $entry, $params);
        $entry->update($params);
        $updatedEntry = $this->getEntry($id);
        $this->triggerChangeEvent('updateEntry', $updatedEntry, $notes);
        return $updatedEntry;
    }

    /**
     * Delete Entry
     *
     * @param int $id
     *
     * @return Model
     * @throws Exception
     */
    public function deleteEntry($id)
    {
        $entry = $this->crudRepositoryTraitModel()->findOrFail($id);
        $entry->delete();
        $this->triggerChangeEvent('deleteEntry', $entry);
        return $entry;
    }

    /**
     * New Model
     *
     * @return Model
     */
    public function newModel()
    {
        return $this->crudRepositoryTraitModel()->newInstance();
    }

    /**
     * New Query
     *
     * @return Model|Builder
     */
    public function newQuery()
    {
        return $this->crudRepositoryTraitModel()->newQuery();
    }

    /* Protected Methods -------------------------------------------------------------------------------------------- */


    /**
     * Load Collection Query
     *
     * @param array $params
     * @param string $orderBy
     * @param Model|Builder $query - Added as a parameter so this method can be extended
     *
     * @return Builder
     */
    protected function loadCollectionQuery($params, $orderBy = null, $query = null)
    {
        // This is done here as well as in higher up methods to make sure it is caught when overriding methods.
        // Isn't intensive so should be ok calling it twice.
        $params = $this->transformParameterCase($params);

        if (empty($query)) {
            $query = $this->newQuery();
        }

        if (array_key_exists('searchTerm', $params) && strlen($params['searchTerm'])) {
            $query = $query->where(function ($q) use ($params) { /** @var Builder $q */
                foreach (empty($this->searchKeys)?[]:$this->searchKeys as $key) {
                    $q->orWhere($key, 'like', '%' . $params['searchTerm'] . '%');
                }
            });
        }

        if (array_key_exists('searchTermRequired', $params)) {
            if (empty($params['searchTermRequired'])) {
                $query = $query->whereNull('id');
            } else {
                $query = $query->where(function ($q) use ($params) { /** @var Builder $q */
                    foreach (empty($this->searchKeys)?[]:$this->searchKeys as $key) {
                        $q->orWhere($key, 'like', '%' . $params['searchTermRequired'] . '%');
                    }
                });
            }
        }

        # In Javascript: "where[$column]": "$value1,$value2",
        if (array_key_exists('where', $params)) {
            foreach ($params['where'] as $column => $values) {
                if ($values == 'null' || $values === null) {
                    $query = $query->whereNull($column);
                } else {
                    $values = is_array($values) ? $values : explode(',', $values);
                    $query = $query->whereIn($column, $values);
                }
            }
        }

        # In Javascript: "whereNot[$column]": "$value1,$value2",
        if (array_key_exists('whereNot', $params)) {
            foreach ($params['whereNot'] as $column => $values) { /** @var string|array $values */
                if ($values == 'null' || $values === null) {
                    $query = $query->whereNotNull($column);
                } else {
                    if (!empty($values)) {
                        $values = is_array($values) ? $values : explode(',', $values);
                        $query = $query->whereNotIn($column, $values);
                    }
                }
            }
        }

//        This is actually misleading, so has been taken out until it can be done correctly.
//        if (array_key_exists('orWhere', $params)) {
//            foreach ($params['orWhere'] as $column => $values) {
//                $values = is_array($values) ? $values : explode(',', $values);
//                $query = $query->orWhereIn($column, $values);
//            }
//        }

        # In Javascript: "whereHas": "$relationship1,$relationship2",
        if (array_key_exists('whereHas', $params)) {
            $whereHas = is_array($params['whereHas']) ? $params['whereHas'] : explode(',', $params['whereHas']);
            foreach ($whereHas as $relationship) {
                $query = $query->has($relationship);
            }
        }

        if (array_key_exists('orderBy', $params)) {
            $orderBy = $params['orderBy'];
        } elseif (is_null($orderBy) && isset($this->defaultOrderBy)) {
            $orderBy = $this->defaultOrderBy;
        }
        if ($orderBy != null) {
            $orderBy = is_array($orderBy) ? $orderBy : explode(',', $orderBy);
            foreach ($orderBy as $order) {
                $bits = explode(' ', $order);
                $query = $query->orderBy($bits[0], empty($bits[1])?'asc':$bits[1]);
            }
        }

        if (array_key_exists('with', $params)) {
            $query = $this->queryWith($query, $params['with']);
        }

        if (array_key_exists('limit', $params)) {
            $query = $query->limit($params['limit']);
        }

        return $query;
    }

    /**
     * Set Now
     *
     * Updates model and also returns updated params, so can be used in both ways.
     *
     * @param string $field
     * @param Model $entry
     * @param array $params
     * @return array
     */
    protected function setNow($field, $entry, $params)
    {
        if (in_array($field, $entry->getFillable()) && !isset($params[$field])) {
            $now = date('Y-m-d H:i:s');
            $entry->$field = $now;
            $params[$field] = $now;
        }
        return $params;
    }

    /**
     * Trigger Change Event
     *
     * This can be overridden if more parameters are required for the event class.
     * (All crud change events need to have the same parameters)
     *
     * @param string $methodName
     * @param Model $entry
     * @param string|null $notes
     */
    protected function triggerChangeEvent($methodName, $entry, $notes = null)
    {
        if ($this->hasEventTrigger($methodName)) {
            if (!$this->muteNextChangeEvent) {
                $event = $this->changeEvents[$methodName];
                /** @var CrudChangeEvent $event */
                event(new $event($entry, auth()->user(), $notes));
            }
            $this->muteNextChangeEvent = false;
        }
    }

    /**
     * Has Event Trigger
     *
     * @param $methodName
     * @return bool
     */
    protected function hasEventTrigger($methodName)
    {
        return (bool) (isset($this->changeEvents)
            && is_array($this->changeEvents)
            && isset($this->changeEvents[$methodName])
        );
    }

    /**
     * Changes
     *
     * @param Model $entry
     * @param array $params
     * @return string
     */
    protected function changes(Model $entry, array $params)
    {
        $changes = [];
        $updated = $this->crudRepositoryTraitModel()->newInstance($params);
        foreach ($params as $k => $v) {
            if (isset($updated->$k) && $entry->$k != $updated->$k) {
                $original = $this->toString($entry->$k);
                $new = $this->toString($updated->$k);
                $changes[] = sprintf('%s changed from "%s" to "%s".', ucfirst($k), $original, $new);
            }
        }
        return implode(" \r\n", $changes);
    }

    /**
     * To String
     *
     * Converts a value to string, used for logging changes
     *
     * @param $item
     * @return string
     */
    protected function toString($item)
    {
        if (is_object($item)
            && (get_class($item) === 'Carbon\Carbon' || is_subclass_of($item, 'Carbon\Carbon'))
            && method_exists($item, 'format')
        ) {
            return $item->format('Y-m-d H:i:s');
        }
        //
        if (is_object($item) || is_array($item)) {
            return json_encode($item);
        }
        // Else
        return (string) $item;
    }

    /**
     * Query With
     *
     * Checks that 'with' values are valid before adding them to the query.
     * This only checks the top level relationships, after that it will just try and throw an exception if not found.
     *
     * @param Builder $query
     * @param $with
     * @return Builder
     */
    protected function queryWith(Builder $query, $with)
    {
        $with = is_array($with) ? $with : explode(',', $with);
        foreach ($with as $w) {
            if (is_null($this->allowedWith) || in_array($w, $this->allowedWith)) {
                if ($this->relationshipExists($this->newModel(), $w)) {
                    $query = $query->with($w);
                }
            }
        }
        return $query;
    }

    /**
     * Relationship Exists
     *
     * Recursive function checking nested relationships exist (e.g. related1.related2)
     *
     * @param $model
     * @param $related
     * @return bool
     */
    protected function relationshipExists($model, $related)
    {
        $bits = explode('.', $related, 2);
        $related = array_shift($bits);
        $nested = implode('.', $bits);
        try {
            $relationship = $model->{$related}(); /** @var Relation $relationship */
            if (is_a($relationship, Relation::class)) {
                if (empty($nested)) {
                    return true;
                } else {
                    return $this->relationshipExists($relationship->newModelInstance(), $nested);
                }
            }
        } catch (BadMethodCallException $e) {
            // Fall through to return false
        }
        return false;
    }

    /**
     * Transform Parameter Case
     *
     * For use with loadCollection, will allow snake_case versions of parameters, nicer for query strings.
     * (e.g. order_by converted to orderBy)
     * Exception for per_page which works the other way, allows perPage
     *
     * @param array $params
     * @return array
     */
    protected function transformParameterCase(array $params)
    {
        foreach ($params as $k => $v) {
            if (($tk = Str::snake($k)) !== 'per_page') {
                $tk = Str::camel($k);
            }
            if ($tk !== $k) {
                $params[$tk] = $v;
                unset($params[$k]);
            }
        }
        return $params;
    }
}
